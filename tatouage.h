#pragma once

struct Carthesian{
    float p;
    float theta;
    float phi;
    unsigned int index;

    Carthesian(){
        p = 0.0f;
        theta = 0.0f;
        phi = 0.0f;
        index = 0;
    }

    Carthesian(float a, float b, float c){
        p = a;
        theta = b;
        phi = c;
        index = 0;
    }
};

struct OperatorTatouage{

    std::vector<std::string> WaterMark;
    std::string Filename;
    std::string Format;

    OperatorTatouage(){
        WaterMark = std::vector<std::string>();
        Filename = "Message";
        Format = ".txt";
    }

    ~OperatorTatouage(){};

        void WriteBinaryToFile(const std::string& _f, const std::string& _format, const std::vector<int>& _Data) {
        std::string _filename = _f + _format; 
        std::ofstream file(_filename, std::ios::app);
        
        if (!file.is_open()) {
            std::cerr << "Erreur : Impossible d'ouvrir le fichier " << _filename << std::endl;
            return;
        }

        for (int bit : _Data) {
            if (bit == 0) {
                file.put('0');
            } 
            else if (bit == 1) {
                file.put('1');
            } 
            else {
                std::cerr << "Erreur : Les données du vecteur ne sont pas valides." << std::endl;
                return;
            }
        }

        file << std::endl;

        file.close();
        std::cout << "Données écrites avec succès dans " << _filename << std::endl;
    }

    void WriteRMSEToFile(const std::string& filename, float _rmse, float _alpha, int _nbins, float _delta) {
        std::ofstream file(filename, std::ios::app); 

        if (file.is_open()) {
            file << "RMSE: " << _rmse
            << " Alpha: " << _alpha 
            << " NBins: " << _nbins
            << " Delta: " << _delta 
            << std::endl;
            file.close();
            std::cout << "Ligne ajoutée dans le fichier " << filename << std::endl;
        } else {
            std::cerr << "Erreur : Impossible d'ouvrir le fichier " << filename << std::endl;
        }
    }


    float RMSE(const std::vector<Vec3>& o_vertices, const std::vector<Vec3>& c_vertices) {
        if (o_vertices.size() != c_vertices.size()) {
            throw std::invalid_argument("Les deux vecteurs doivent avoir la même taille.");
        }

        float squaredError = 0.0f;

        for (size_t i = 0; i < o_vertices.size(); i++) {
            float errorX = c_vertices[i][0] - o_vertices[i][0];
            float errorY = c_vertices[i][1] - o_vertices[i][1];
            float errorZ = c_vertices[i][2] - o_vertices[i][2];
            
            squaredError += errorX * errorX + errorY * errorY + errorZ * errorZ;
        }

        float meanSquaredError = squaredError / (float)(o_vertices.size());
        float rmse = std::sqrt(meanSquaredError);

        return rmse;
    }

    Vec3 barycentre(std::vector<Vec3> _vertices){
        Vec3 _means(0.,0.,0.);
        for(unsigned int i = 0 ; i < _vertices.size() ; ++i){
            _means+=_vertices[i];
        }
        _means /= _vertices.size();
        return _means;
    }

    std::vector<Carthesian> ToSpherical(std::vector<Vec3> _vertices){
        Vec3 centre = barycentre(_vertices);
        std::vector<Carthesian> res;
        res.resize(_vertices.size());
        for(unsigned int i = 0 ; i < _vertices.size(); ++i){
            res[i].p = std::sqrt(std::pow(_vertices[i][0]-centre[0],2)+std::pow(_vertices[i][1]-centre[1],2)+std::pow(_vertices[i][2]-centre[2],2));
            res[i].theta = std::atan2((float)(_vertices[i][1] - centre[1]),(float)(_vertices[i][0] - centre[0]));
            res[i].phi = std::acos((_vertices[i][2] - centre[2])/(std::sqrt(std::pow(_vertices[i][0]-centre[0],2)+std::pow(_vertices[i][1]-centre[1],2)+std::pow(_vertices[i][2]-centre[2],2))));
            res[i].index = i;
        }
        return res;
    }

    std::vector<Vec3> ToCarthesian(std::vector<Carthesian> _c, Vec3 _centre){
        std::vector<Vec3> res;
        res.resize(_c.size());
        for(unsigned int i = 0 ; i < _c.size() ; ++i){
            Carthesian _vertex;
            for(unsigned int j = 0; j < _c.size() ; ++j){
                if( _c[j].index == i){
                    _vertex = _c[j];
                    break;
                }
            }
            res[i][0] = _vertex.p * std::cos(_vertex.theta) * std::sin(_vertex.phi ) + _centre[0];
            res[i][1] = _vertex.p * std::sin(_vertex.theta) * std::sin(_vertex.phi ) + _centre[1];
            res[i][2] = _vertex.p * std::cos(_vertex.phi) + _centre[2];
        }
        return res;
    }

    float GetMin(std::vector<Carthesian> _cart){
        float _min = FLT_MAX;
        for(Carthesian _c : _cart){
            _min = std::min<float>(_min,_c.p);
        }
        return _min;
    }

    std::vector<float> GetAllMin(std::vector<std::vector<Carthesian>> _Bins){
        std::vector<float> _min(_Bins.size(),FLT_MAX);
        for(unsigned int i = 0; i < _Bins.size() ; ++i){
            for(Carthesian _c : _Bins[i]){
                _min[i] = std::min<float>(_min[i],_c.p);
            }
        }
        return _min;
    }

    float GetMax(std::vector<Carthesian> _cart){
        float _max = -FLT_MAX;
        for(Carthesian _c : _cart){
            _max = std::max<float>(_max,_c.p);
        }
        return _max;
    }

    std::vector<float> GetAllMax(std::vector<std::vector<Carthesian>> _Bins){
        std::vector<float> _max(_Bins.size(),-FLT_MAX);
        for(unsigned int i = 0; i < _Bins.size() ; ++i){
            for(Carthesian _c : _Bins[i]){
                _max[i] = std::max<float>(_max[i],_c.p);
            }
        }
        return _max;
    }

    float GetMean(const std::vector<Carthesian>& _Bin){
        float _means = 0.0f;
        for(unsigned int i = 0 ; i < _Bin.size() ; ++i){
            _means += _Bin[i].p;
        }
        _means /= (_Bin.size() != 0 ? (float)(_Bin.size()) : 1.0f);
        return _means;
    }

    std::vector<float> GetMeans(const std::vector<std::vector<Carthesian>>& _Bins){
        std::vector<float> _means(_Bins.size());
        for(unsigned int i = 0 ; i < _Bins.size() ; ++i){
            _means[i] = GetMean(_Bins[i]);
        }
        return _means;
    }

    void Normalize(std::vector<std::vector<Carthesian>>& _Bins, std::vector<float> _min, std::vector<float> _max){
        for(unsigned int i = 0 ; i < _Bins.size() ; ++i){
            for(unsigned int j = 0 ; j < _Bins[i].size() ; ++j){
                _Bins[i][j].p = (_Bins[i][j].p - _min[i])/(std::max(_max[i] - _min[i],0.001f));
            }
        }
    }

    void UndoNormalize(std::vector<std::vector<Carthesian>>& _Bins, std::vector<float> _min, std::vector<float> _max) {
        for(unsigned int i = 0; i < _Bins.size(); ++i) {
            for(unsigned int j = 0; j < _Bins[i].size(); ++j) {
                _Bins[i][j].p = _Bins[i][j].p * (std::max(_max[i] - _min[i], 0.001f)) + _min[i];
            }
        }
    }

    std::vector<std::vector<Carthesian>> GetBins(std::vector<Carthesian> _f, float _min, float _max, int _nbins=1){
        std::vector<std::vector<Carthesian>> Bins;
        Bins.resize(_nbins);
        float _local_min;
        float _local_max;
        int ct=0;
        for (unsigned int n = 0; n < _f.size(); ++n) { 
            for (int l = 0; l < _nbins ; ++l) {
                _local_min = _min + ((_max - _min) / _nbins) * l;
                _local_max = _min + ((_max - _min) / _nbins) * (l + 1);
                //std::cout << _local_min << " <= " << _f[n].p << " <= " <<_local_max << std::endl;
                if (_f[n].p >= _local_min && _f[n].p <= _local_max) { 
                    Bins[l].push_back(_f[n]); 
                    ct++;
                }
            }
        }
        std::cout << "Bins contains " << ct << "/" << _f.size() << "vertices" <<std::endl;
        return Bins;
    }

    std::vector<Carthesian> GetPoint(const std::vector<std::vector<Carthesian>> _Bins){
        std::vector<Carthesian> _c;
        for(unsigned int i = 0 ; i<_Bins.size(); ++i){
            for(unsigned int j = 0 ; j<_Bins[i].size(); ++j){
                _c.push_back(_Bins[i][j]);
            }
        }
        return _c;
    }

    void TransformBin(std::vector<Carthesian>& _bin, float k) {
        for (unsigned int i = 0; i < _bin.size(); i++) {
            _bin[i].p = std::pow(_bin[i].p, k);
        }
    }

    void RotateBins(std::vector<std::vector<Carthesian>>& _Bins, float alpha, float force = 1.0f, unsigned int _iter = 100){
        float _k = 1.0f;
        float _means = 0.0f;
        unsigned int _count = 0;
        for(unsigned int w = 0; w < this->WaterMark.size(); ++w){

            std::cout << "Operate WaterMark number " 
            << w << "/" << WaterMark.size() 
            << " => " << this->WaterMark[w] << "...begin..." << std::endl;

            _count = 0;
            _k = 1.0f;
            _means = GetMean(_Bins[w]);
            bool _watermark_direction = (WaterMark[w] == "1" ? true : false);

            std::cout << "Going : " << (_watermark_direction == true ? " +w ---> " : " -w ---> ");

            std::cout << "Firstly "<< _means << (_watermark_direction == true ? " >= " : " <= ") 
            << (_watermark_direction == true ? (float)(1./2. + alpha) : (float)(1./2. - alpha)) 
            << std::endl ; 

            //std::vector<Carthesian> _Save_bins(_Bins[w]);
            
                if(_watermark_direction){
                    std::cout << " @@@@ +++w++ @@@@" << std::endl;
                    while(_means <= (float)(1./2. + alpha) && _count < _iter){
                        std::cout << _means << "---->" ; 
                        //_Bins[w] = std::vector<Carthesian>(_Save_bins);
                        TransformBin(_Bins[w], _k);
                        _means = GetMean(_Bins[w]);
                        std::cout << _means; 
                        std::cout << " >= "<<(float)(1./2. + alpha) << std::endl ; 
                        _k-=force;
                        ++_count;
                    }
                }
                else{
                    std::cout << " @@@@ ---w-- @@@@" << std::endl;
                    while(_means > (float)(1./2. - alpha) && _count < _iter){
                        std::cout << _means << "---->" ; 
                        //_Bins[w] = std::vector<Carthesian>(_Save_bins);
                        TransformBin(_Bins[w], _k);
                        _means = GetMean(_Bins[w]);
                        std::cout << _means; 
                        std::cout << " <= "<<(float)(1./2. - alpha) << std::endl ; 
                        _k+=force;
                        ++_count;
                    }
                }

            std::cout << "Operate WaterMark number " 
            << w+1 << "/" << WaterMark.size() 
            << " ...done. \n" << std::endl;
        }
    }

    std::vector<Vec3> insertion(std::vector<Vec3> _maillage, float _alpha = 0.2f, int _nbins=1, float _force=1.0f){
        std::cout << "------begin init---------" << std::endl;

        Vec3 _centre = barycentre(_maillage);
        std::vector<Carthesian> _cart = ToSpherical(_maillage);

        std::cout << "------end init---------" << std::endl;

        std::cout << "------begin init bins---------" << std::endl;

        float _min = GetMin(_cart);
        float _max = GetMax(_cart);
        std::cout << _min << "," << _max << std::endl;
        std::vector<std::vector<Carthesian>> _Bins = GetBins(_cart,_min,_max,_nbins);

        /*
           Normalize
        */

        

        std::vector<float> _min_bin = GetAllMin(_Bins);
        std::vector<float> _max_bin = GetAllMax(_Bins);
        Normalize(_Bins,_min_bin,_max_bin);
        
        std::cout << "------end init bins---------" << std::endl;

        /*
          WaterMark
        */

        std::cout << "------begin operate---------" << std::endl;
        RotateBins(_Bins, _alpha, _force);
        std::cout << "------end operate---------" << std::endl;
        
        std::cout << "------begin insertion---------" << std::endl;

        /*
           Inverse Normalize
        */

        UndoNormalize(_Bins,_min_bin,_max_bin);
        std::vector<Carthesian> _c = GetPoint(_Bins);
        std::vector<Vec3> _new_maillage = ToCarthesian(_c,_centre);
        std::cout << "------end insertion---------" << std::endl;

        return _new_maillage;
    }

    void extraction(const std::vector<Vec3>& _maillage, float _alpha = 0.2f, int _nbins=1){

        std::cout << "------begin init---------" << std::endl;

        std::vector<Carthesian> _cart = ToSpherical(_maillage);

        std::cout << "------end init---------" << std::endl;

        std::cout << "------begin init bins---------" << std::endl;

        float _min = GetMin(_cart);
        float _max = GetMax(_cart);
        std::cout << _min << "," << _max << std::endl;
        std::vector<std::vector<Carthesian>> _Bins = GetBins(_cart,_min,_max,_nbins);

        /*
           Normalize
        */

        std::vector<float> _min_bin = GetAllMin(_Bins);
        std::vector<float> _max_bin = GetAllMax(_Bins);
        Normalize(_Bins,_min_bin,_max_bin);

        std::cout << "------end init bins---------" << std::endl;

        std::cout << "------begin extraction---------" << std::endl;

        std::vector<float> _means = GetMeans(_Bins);
        std::vector<int> message;
        for(unsigned int i = 0 ; i < std::min(WaterMark.size(),_means.size()) ; ++i){
            std::cout << "Actual means[" << i << "] = " << _means[i] << std::endl;
            message.push_back((_means[i] > (float)(1./2.) ? 1 : 0));
        }

        std::cout << "------end extraction---------" << std::endl;

        std::cout << "------begin writing---------" << std::endl;

        WriteBinaryToFile(Filename,Format,message);

        std::cout << "------end writing---------" << std::endl;

        return;
    }

};